# CFPT-Velohydro

Système Web permettant la réception, le stockage et l'affichage des données GPS des vélos à hydrogène du CFPT.

Auteur : [Tom Andrivet~CanardConfit](https://canardconfit.ch)

## Installation

Le projet est fait en NodeJS. Pour l'installation, il suffit de :
- Mettre a jour les dépendances (```npm install```).
- Ouvrir le projet avec votre IDE (IDE de base, [WebStorm](https://www.jetbrains.com/fr-fr/webstorm/)).
- Setup une configuration de lancement qui lancera la commande ```npm start```.

## Installation Production

Pour installer le système sur un environnement utilisateur :
- Le serveur doit être muni de NodeJS.
- Installez le projet dans un dossier et lancez ```npm install```.
- Pour lancer le serveur, utilisez ```npm start prod```.
- Suivez ensuite les instructions en console et sur le serveur web nodejs.

## Fonctionnalité

- Statistique des données récupérées.
- Support de la vitesse actuelle d'un vehicule (donné à transmettre).
- Permets de voir les données brutes de la base de données.
- Map globale permettant de visualiser tous les trajets des véhicules.
- Gestion des utilisateurs (Ajout, suppression, modification).
    - Identifiant.
    - Pseudo.
    - Mot de passe.
- Gestion des véhicules (Ajout, suppression, modification).
    - Couleur.
    - Nom.
- Voir le trajet individuel d'un véhicule.
    - Possibilité d'ajouter des données à un trajet.
- Système d'installation manuelle de la base de données.
    - Entrée des identifiants de connexion.
    - Importation des tables de la base de données.
- Gestion des erreurs critiques (fatales).
- Portail d'ajout de données à un véhicule donné.

## Portail d'ajout de données

Le protail marche de la façon suivante, vous devez executer une requete HTTP(S) sur cette route : ```[racine du site]/portal```.
La requete doit contenir une variable ```request``` avec du JSON, voici un exemple :

```JSON
{
  "func":"addLoc",
  "params": {
    "lat":"10",
    "lng":"10",
    "date":"2020-12-02 13:20:00",
    "vehiculename":"test"
  }
}

```
- **func** : Fonction utilisée
    - ```addLoc``` : Permet d'ajouter une localisation a un vehicule.
- **params** : Contient les paramètres spécifique pour la fonction choisie.
    - **addLoc** :
        - ```lat``` : Latitude de la coordonnée a enregistrée.
        - ```lng``` : Longitude de la coordonnée a enregistrée.
        - ```date``` : [Optionnelle, met la date actuelle si vide] date de la prise de coordonnée (Format ```YYYY-MM-DD hh:mm:ss```).
        - ```vehiculename``` : Nom du vehicule recevant la nouvelle position.

### Réponse réussite reçue

```JSON
{
  "status":"SUCCESS",
  "msg":"Successfully added point at [x] | [x] for [vehicule]."
}
```

### Les réponses en cas d'erreur

Erreur quand la variable ```func``` n'est pas présente ou vide.
```JSON
{
  "status":"ERROR",
  "msg":"Error func variable JSON doesn't exist."
}
```

Erreur quand le JSON est malformé.
```JSON
{
  "status":"ERROR",
  "msg":"Error JSON malformed."
}
```
Erreur quand la variable ```params``` n'est pas présente ou vide.
```JSON
{
  "status":"ERROR",
  "msg":"Error params variable JSON doesn't exist."
}
```
Erreur quand une variable obligatoire pour la fonction choisie n'est pas présente ou vide dans ```params```.
```JSON
{
  "status":"ERROR",
  "msg":"Error parameters for function doesn't exist."
}
```