/*

Projet : VeloHydro
Description : Application principale Express

 */

/*

Dépendances de démarrage Express

 */
let createError = require('http-errors');
let express = require('express');
let path = require('path');
let session = require('express-session');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let settings = require('./settings');

/*

Route de l'application

 */
let setupRouter = require('./routes/setup');
let indexRouter = require('./routes/index');
let tablesRouter = require('./routes/tables');
let mapRouter = require('./routes/map');
let usersRouter = require('./routes/users');
let vehiculesRouter = require('./routes/vehicules');

/*

Initialisation d'Express

 */

let app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(session({
  secret: 'Ytm52jAtFBxVz9U4',
  resave: true,
  saveUninitialized: true
}));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


/*

Initialisation des routes

 */
if (settings.isSetup()) {
  app.use('/', indexRouter);
  app.use('/tables', tablesRouter);
  app.use('/map', mapRouter);
  app.use('/users', usersRouter);
  app.use('/vehicules', vehiculesRouter);
} else {
  console.log("[SETUP] Ceci est le premier lancement, veuillez suivre l'installation en ligne sur l'application.");
  app.use('/', setupRouter);
}

// Initialisation des erreurs.
app.use(function(req, res, next) {
  res.render("error");
});

module.exports = app;
