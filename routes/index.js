/*

Projet : VeloHydro
Description : Route de l'accueil et options

 */
let express = require('express');
let router = express.Router();
let db = require("../database");
let settings = require("../settings");
let dateformat = require("dateformat");

// Route d'accueil
router.get('/', function(req, res, next) {

  if (!settings.isAutorised(req.session, "/", res)) {return;}

  db.getLocs(function(locs) {
    db.getUsers(function(users) {
      db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
        if (isUser) {
          let map = [];

          vehicules.forEach((v) => {
            let points = [];
            locs.forEach((point) => {
              if (v.idVehicule === point.idVehicule) {
                points.push(point);
              }
            });
            map.push(points);
          });

          res.render('index', {speed: settings.getSpeedEnabled(), activate: "accueil", vehicules: vehicules, data: map, session: userSQL, locs: locs, users: users});
        }
      });
    });
  });
});

// Route permettant de modifier les options du programme
router.get("/options/:option/:value", function(req, res, next) {

  if (!settings.isAutorised(req.session, "/", res)) {return;}

  let option = req.params.option;
  let value = req.params.value;

  switch (option) {

    case "speed":
      settings.setSpeedEnabled(value);
      break;
  }

  res.redirect("/");
});

router.get("/portal", function (req, res, next) {

  if (req.query.request) {
    try {
      let reqJson = JSON.parse(req.query.request);

      if (reqJson.func) {
        switch (reqJson.func) {
          case "addLoc":
            if (reqJson.params) {
              if (reqJson.params.lat && reqJson.params.lng && reqJson.params.vehiculename) {
                let lat = reqJson.params.lat;
                let lng = reqJson.params.lng;
                let dateLoc = reqJson.params.date;

                if (!dateLoc) dateLoc = dateformat(new Date(), "yyyy-mm-dd HH:MM:ss");

                let vehiculename = reqJson.params.vehiculename;

                db.getIsVehicule(vehiculename, function (vehiculeSQL, isVehicule) {
                  if (isVehicule) {
                    db.createLoc(lat, lng, dateLoc, vehiculeSQL.idVehicule, function () {

                      res.send(`{"status":"SUCCESS","msg":"Successfully added point at ${lat} | ${lng}."}`);
                    });
                  } else
                    res.send('{"status":"ERROR","msg":"Error vehicule name doesn\'t exist."}');
                });

              } else
                res.send('{"status":"ERROR","msg":"Error parameters for function doesn\'t exist."}');
            } else
              res.send('{"status":"ERROR","msg":"Error params variable JSON doesn\'t exist."}');
            break;
          default:
            res.send('{"status":"ERROR","msg":"Error unknown function."}');
            break;
        }
      } else
        res.send('{"status":"ERROR","msg":"Error func variable JSON doesn\'t exist."}');
    } catch (e) {
      res.send('{"status":"ERROR","msg":"Error JSON malformed."}');
    }
  } else
    res.send('{"status":"ERROR","msg":"Error no request variable."}');
});

module.exports = router;
