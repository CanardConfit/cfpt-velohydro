/*

Projet : VeloHydro
Description : Module d'option du programme

 */
const fs = require('fs');
const FILE = "settings.json";

/*

Initialisation du module

 */
let connected = false;
let settings = JSON.parse(fs.readFileSync(FILE));

// Fonction qui permet de sauvegarder les changements dans le fichier settings.
function save() {
    fs.writeFileSync(FILE, JSON.stringify(settings, null,  1));
}

/*

Fonction exportée du module

 */

// Récupère si l'application a été setup.
module.exports.isSetup = function () {
    return settings.setup;
}

// Récupère la version de l'application.
module.exports.getVersion = function () {
    return settings.version;
}

// Set le status du setup de l'application.
module.exports.setSetup = function (state) {
    settings.setup = state;
    save();
}

// Récuperation des informations de connexion a la base de données.
module.exports.getdbInfo = function () {
    return settings.dbinfo;
}

// Set les informations de connexion a la base de données.
module.exports.setdbInfo = function (state) {
    settings.dbinfo = state;
    save();
}

// Récupère si la gestion de la vitesse des véhicules est activée.
module.exports.getSpeedEnabled = function () {
    return settings.speedenabled;
}

// Set le status de la gestion de la vitesse des véhicules.
module.exports.setSpeedEnabled = function (state) {
    settings.speedenabled = state === "true";
    save();
}

// Récupère si la base de données est connectée.
module.exports.isConnected = function () {
    return connected;
}

// Set le status de connexion a la base de données.
module.exports.setConnected = function (state) {
    connected = state;
}

// Fonction qui verifie si l'on est autorisé a accèder au programme normal (erreur fatale ou non)
module.exports.isAutorised = function (session, redirect, res) {
    if (!connected) {
        res.render("fatalerror", {head: "Erreur de la base de données", body: "La base de données n'est pas connectées. Veuillez relancer le serveur nodejs et verifier les erreurs en console."});
        res.end();
        return false;
    } else if (!session.loggedin) {
        res.render("users/login", {link: redirect});
        res.end();
        return false;
    }
    return true;
}