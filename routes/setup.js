/*

Projet : VeloHydro
Description : Route pour le setup de l'application (Base de données)

 */
var express = require('express');
var app = require('../app');
var router = express.Router();
var db = require("../database");
var settings = require('../settings');

/*

Enregistrement des routes

 */

// Redirection du root a l'installation
router.get('/', function(req, res, next) {
    if (settings.isSetup()) return;
    res.redirect("/setup/installation");
});

// Route pour l'affichage du setup
router.get('/setup/installation', function(req, res, next) {
    if (settings.isSetup()) return;
    res.render("setup/installation", {dbConnected: settings.isConnected()});
});

// Route pour la validation de la base de données (JSON response)
router.post('/setup/database', function(req, res, next) {
    if (settings.isSetup()) return;
    let host = req.body.host;
    let port = req.body.port;
    let username = req.body.username;
    let password = req.body.password;
    let database = req.body.database;

    if (host !== "" && port !== "" && username !== "" && database !== "") {

        settings.setdbInfo({
            host: host,
            port: port,
            username: username,
            password: password,
            database: database
        });

        db.connection(function (state, msg) {
            if (state) {
                res.send('{"state":"true","msg":"Connexion à la base de données réussi !"}');
            } else {
                res.send('{"state":"false","msg":"Quelque chose n\'a pas marché : ' + msg + '"}');
            }
        });
    } else {
        res.send('{"state":"false","msg":"Un des champs obligatoires n\'est pas rempli !"}');
    }
});

// Route pour l'importation du script de base de données (JSON response)
router.post('/setup/import', function(req, res, next) {

    db.importFile("sql-script/database.sql", function (state, msg) {
        if (state) {
            settings.setSetup(true);
            res.send('{"state":"true","msg":"Importation de la base de données réussi !"}');
        } else {
            res.send('{"state":"false","msg":"Quelque chose n\'a pas marché : ' + msg + '"}');
        }
    });
});

// Route pour la finalisation et l'arrêt du serveur NodeJS
router.post('/setup/finished', function(req, res, next) {
    console.log("Fermeture du programme après l'installation, ceci est normal, veuillez le relancer.");
    process.exit(0);
});

module.exports = router;