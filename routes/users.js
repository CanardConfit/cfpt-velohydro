/*

Projet : VeloHydro
Description : Route pour la gestion des utilisateurs

 */
let express = require('express');
let passwordHash = require('password-hash');
let router = express.Router();
let db=require('../database');
let settings=require('../settings');

/*

Enregistrement des routes

 */

// Route pour authentifier un utilisateur (JSON response).
router.post('/auth', function(req, res, next) {

    let user = req.body.username;
    let password = req.body.password;
    if (user && password) {
        db.getIsUser(user,function (userSQL, isUser) {
            if (isUser && passwordHash.verify(password, userSQL.hashUser)) {
                req.session.loggedin = true;
                req.session.username = user;
                res.send('{"state":"true","msg":"Vous avez été connecté avec succès"}');
            } else {
                res.send('{"state":"false","msg":"Mot de passe ou utilisateur incorrect !"}');
            }
        });
    } else {
        res.send('{"state":"false","msg":"Merci d\'entrer votre utilisateur et votre mot de passe !"}');
    }
});

// Route pour déconnecter un utilisateur (Redirection).
router.get('/logout', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/", res)) {return;}
    req.session.loggedin = false;
    req.session.username = "";

    res.redirect("/");
});

// Route pour la création d'un utilisateur (JSON response).
router.post('/register', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/", res)) {return;}

    let user = req.body.username;
    let password = req.body.password;
    let pseudo = req.body.pseudo;
    if (user && password && pseudo) {
        db.getIsUser(user, function(userSQL, isUser) {
            if (!isUser) {
                db.createUser(user, passwordHash.generate(password), pseudo, function () {
                    res.send('{"state":"true","msg":"Utilisateur créer, vous pouvez a présent vous connecter avec celui-ci"}');
                });
            } else {
                res.send('{"state":"false","msg":"Le nom d\'utilisateur est déjà utilisé !"}');
            }
        });
    } else {
        res.send('{"state":"false","msg":"Merci d\'entrer un nom d\'utilisateur, un pseudo et un mot de passe !"}');
    }
});

// Route pour la suppression d'un utilisateur (JSON response).
router.post('/delete', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/", res)) {return;}

    let user = req.body.username;
    if (user) {
        db.getIsUser(user, function(userSQL, isUser) {
            if (isUser) {
                db.deleteUser(user, function() {
                    res.send('{"state":"true","msg":"Utilisateur supprimé."}');
                });
            } else {
                res.send('{"state":"false","msg":"Merci d\'entrer un nom d\'utilisateur existant !"}');
            }
        });
    } else {
        res.send('{"state":"false","msg":"Merci d\'entrer un nom d\'utilisateur à supprimer !"}');
    }
});

// Route pour l'affichage de la page d'ajout/suppression/modification.
router.get('/manage', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/users/manage", res)) {return;}

    db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
        if (isUser) {
            res.render("users/manage",{vehicules: vehicules, activate: "usermanage", session: userSQL});
        }
    });
});

// Route pour la modification d'un utilisateur (JSON response).
router.post('/modify/:func', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/", res)) {return;}

    if (req.params.func === "pass") {
        let user = req.body.user;
        let pass1 = req.body.pass1;
        let pass2 = req.body.pass2;
        if (user && pass1 && pass2) {
            db.getIsUser(user, function (userSQL, isUser) {
                if (isUser) {
                    if (pass1 === pass2) {
                        db.updatePassUser(passwordHash.generate(pass1), user, function () {
                            res.send('{"state":"true","msg":"Mot de passe de l\'utilisateur modifié !"}');
                        });
                    } else {
                        res.send('{"state":"false","msg":"Les deux mots de passe ne sont pas identiques !"}');
                    }
                } else {
                    res.send('{"state":"false","msg":"L\'utilisateur n\'existe pas !"}');
                }
            });
        } else {
            res.send('{"state":"false","msg":"Merci d\'entrer un mot de passe et de le repeter !"}');
        }
    } else if (req.params.func === "info") {
        let user = req.body.user;
        let pseudo = req.body.pseudo;
        let username = req.body.username;
        if (user && pseudo && username) {
            db.getIsUser(user, function (userSQL, isUser) {
                if (isUser) {
                    db.updateInfoUser(user, pseudo, username, function () {
                        res.send('{"state":"true","msg":"Information de l\'utilisateur modifié !"}');
                    });
                } else {
                    res.send('{"state":"false","msg":"L\'utilisateur n\'existe pas !"}');
                }
            });
        } else {
            res.send('{"state":"false","msg":"Des champs sont vide !"}');
        }
    }
});

// Route pour l'affichage de la modification d'un utilisateur.
router.get('/modify', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/users/modify", res)) {return;}

    db.getIsUser(req.query.user, function (userModSQL, isUserMod) {
        if (isUserMod) {
            db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
                if (isUser) {
                    res.render("users/modify", {vehicules: vehicules, activate: "usermanage", session: userSQL, userMod: userModSQL});
                }
            });
        } else {
            res.redirect("/users/manage");
        }
    });
});

// Route qui verifie si un utilisateur existe (JSON response).
router.post('/exist', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/", res)) {return;}

    db.getIsUser(req.body.user, function (userSQL, isUser) {
        if (isUser) {
            res.send('{"state":"true","msg":"Utilisateur exist"}');
        } else {
            res.send('{"state":"false","msg":"L\'utilisateur n\'existe pas !"}');
        }
    });
});

module.exports = router;