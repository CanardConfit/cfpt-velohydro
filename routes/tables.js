/*

Projet : VeloHydro
Description : Route pour visualisation des tables brutes de la base de données

 */
let express = require('express');
let router = express.Router();
let db=require('../database');
let settings=require('../settings');

/*

Enregistrement des routes

 */

// Route pour l'affichage de la table locs.
router.get('/locs', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/tables/locs", res)) {return;}

    db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
        if (isUser) {
            db.getLocs(function (locs) {
                res.render('table/locs', {speed: settings.getSpeedEnabled(), locData: locs, vehicules: vehicules, activate: "tablelocs", session: userSQL});
            });
        }
    });
});

// Route pour l'affichage de la table vehicules.
router.get('/vehicules', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/tables/vehicules", res)) {return;}

    db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
        if (isUser) {
            res.render('table/vehicules', {vehicules: vehicules, activate: "tablevehicules", session: userSQL});
        }
    });
});

// Route pour l'affichage de la table users.
router.get('/users', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/users/add", res)) {return;}

    db.getUsers(function (users) {
        db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
            if (isUser) {
                res.render("table/users",{vehicules: vehicules, activate: "tableusers", session: userSQL, users: users});
            }
        });
    });
});

module.exports = router;