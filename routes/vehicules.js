/*

Projet : VeloHydro
Description : Route pour la gestion des vehicules

 */
let express = require('express');
let router = express.Router();
let db=require('../database');
let settings=require('../settings');

/*

Enregistrement des routes

 */

// Route pour la création d'un vehicule (JSON response).
router.post('/register', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/", res)) {return;}

    let name = req.body.name;
    let color = req.body.color;
    if (name && color) {
        db.getIsVehicule(name, function (vehiculeSQL, isVehicule) {
            if (!isVehicule) {
                db.createVehicule(name, color, function () {
                    res.send('{"state":"true","msg":"Vehicule créer !"}');
                });
            } else {
                res.send('{"state":"false","msg":"Le nom du vehicule est déjà utilisé !"}');
            }
        });
    } else {
        res.send('{"state":"false","msg":"Merci d\'entrer un nom de vehicule et une couleur !"}');
    }
});

// Route pour la suppression d'un vehicule (JSON response).
router.post('/delete', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/", res)) {return;}

    let vehicule = req.body.name;
    if (vehicule) {
        db.getIsVehicule(vehicule, function (vehiculeSQL, isVehicule) {
            if (isVehicule) {
                db.deleteVehicule(vehicule, function () {
                    res.send('{"state":"true","msg":"Vehicule supprimé."}');
                });
            } else {
                res.send('{"state":"false","msg":"Merci d\'entrer un nom de vehicule existant !"}');
            }
        });
    } else {
        res.send('{"state":"false","msg":"Merci d\'entrer un nom de vehicule à supprimer !"}');
    }
});

// Route pour l'affichage de la page d'ajout/suppression/modification.
router.get('/manage', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/vehicules/manage", res)) {return;}

    db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
        if (isUser) {
            res.render("vehicules/manage", {vehicules: vehicules, activate: "usermanage", session: userSQL});
        }
    });
});

// Route pour la modification d'un vehicule (JSON response).
router.post('/modify/:func', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/", res)) {return;}

    if (req.params.func === "info") {
        let nameold = req.body.nameold;
        let name = req.body.name;
        let color = req.body.color;
        if (nameold && name && color) {
            db.getIsVehicule(nameold, function (vehiculeSQL, isVehicule) {
                if (isVehicule) {
                    db.updateInfoVehicule(nameold, color, name, function () {
                        res.send('{"state":"true","msg":"Information du vehicule modifié !"}');
                    });
                } else {
                    res.send('{"state":"false","msg":"Le vehicule n\'existe pas !"}');
                }
            });
        } else {
            res.send('{"state":"false","msg":"Des champs sont vide !"}');
        }
    }
});

// Route pour l'affichage de la modification d'un vehicule.
router.get('/modify', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/vehicules/modify", res)) {return;}

    db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
        if (isUser) {
            let vehiculeMod;
            for (let v of vehicules)
                if (v.nomVehicule === req.query.name)
                    vehiculeMod = v;

            if (vehiculeMod) {
                res.render("vehicules/modify", {
                    vehicules: vehicules,
                    activate: "vehiculemanage",
                    session: userSQL,
                    vehiculeMod: vehiculeMod
                });
            } else {
                res.redirect("/vehicules/manage");
            }
        }
    });
});

// Route qui verifie si un vehicule existe (JSON response).
router.post('/exist', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/", res)) {return;}

    db.getIsVehicule(req.body.name, function (vehiculeSQL, isVehicule) {
        if (isVehicule) {
            res.send('{"state":"true","msg":"Vehicule exist"}');
        } else {
            res.send('{"state":"false","msg":"Le Vehicule n\'existe pas !"}');
        }
    });
});

module.exports = router;