/*

Projet : VeloHydro
Description : Module de gestion de la base de données du programme

 */
let mysql = require('mysql');
let settings = require('./settings');


/*

Initialisation du module

 */

// Fonction de connexion a la base de données
let conn = null;

// Fonction qui permet de gérer les erreurs causées par les fonctions SQL.
function handleError(err) {
    throw err;
}

// Fonction pour le formattage de la requète SQL de l'ajout des points a un vehicule.
function toStringQuery(points, idVehicule) {
    let string = "";
    points.forEach((point) => {
        if (string !== "")
            string += ", ";
        string += `(NOW(), ${point.lat}, ${point.lng}, ${idVehicule})`;
    });
    return string;
}

/*

Fonction exportée du module

 */

// Fonction qui permet d'importer un fichier SQL dans la base de données
module.exports.importFile = function (file, callback) {
    let lines = require('fs').readFileSync(file, 'utf-8').split('\n');
    let success = true;
    let lineNum = 1;
    for (let line of lines) {
        if (line !== "") {
            conn.query(line.toString(), function (err) {
                lineNum += 1;
                if (err) {
                    if (callback) {
                        callback(false, err.code);
                        success = false;
                    } else
                        throw err;
                }

                if (lineNum >= lines.length && success) {
                    callback(true, "");
                }
            });
        }
    }
}

// Fonction qui permet de ce connecter à la base de données (Peut échoué)
module.exports.connection = function (callback) {

    conn = mysql.createConnection({
        multipleStatements: true,
        host: settings.getdbInfo().host,
        port: settings.getdbInfo().port,
        user: settings.getdbInfo().username,
        password: settings.getdbInfo().password,
        database: settings.getdbInfo().database
    });
    if (callback) {
        conn.connect(function(err) {
            if (err) {
                callback(false, err.code);
                return;
            } else {
                callback(true, "");
            }

            settings.setConnected(true);
            console.log('Base de données connecté !');
        });
    } else {
        conn.connect(function(err) {
            if (err) {
                switch (err.code) {
                    case "ECONNREFUSED":
                    case "ER_ACCESS_DENIED_ERROR":
                    case "ER_BAD_DB_ERROR":
                        if (!settings.isSetup()) {
                            console.log("[DATABASE] La base de données est inactive en raison d'une erreur, elle pourra être configurée lors de l'installation.");
                            return;
                        }

                        console.error("[DATABASE] La base de données n'est pas connectée :");
                        console.error(err.code + " | " + err.message);
                        settings.setConnected(false);
                        return;
                    default:
                        throw err;
                }
            }
            settings.setConnected(true);
            console.log('Base de données connecté !');
        });
    }
}

// Fonction qui permet de récupérer l'utilisateur si il existe.
module.exports.getIsUser = function (user, callback) {
    conn.query('SELECT * FROM user WHERE idUser = ?;', [user], function(err, results) {
        if (err) {
            handleError(err);
            return;
        }
        if (results.length === 1)
            callback(results[0], true);
        else
            callback(null, false);
    });
}

// Fonction qui permet de créer un utilisateur.
module.exports.createUser = function (user, hash, pseudo, callback) {
    conn.query('INSERT INTO user(idUser,hashUser,pseudoUser) VALUES (?,?,?);', [user, hash, pseudo], function (err) {
        if (err) {
            handleError(err);
            return;
        }
        callback();
    });
}

// Fonction qui permet de supprimer un utilisateur.
module.exports.deleteUser = function (user, callback) {
    conn.query('DELETE FROM user WHERE idUser = ?;', [user], function (err) {
        if (err) {
            handleError(err);
            return;
        }
        callback();
    });
}

// Fonction qui permet de mettre à jour les informations de l'utilisateur.
module.exports.updateInfoUser = function (user, pseudo, idUser, callback) {
    conn.query('UPDATE user SET pseudoUser = ?, idUser = ? WHERE idUser = ?;', [pseudo, idUser, user], function (err) {
        if (err) {
            handleError(err);
            return;
        }
        callback();
    });
}

// Fonction qui permet de mettre à jour le mot de passe.
module.exports.updatePassUser = function (user, pass, callback) {
    conn.query('UPDATE user SET hashUser = ? WHERE idUser = ?;', [pass, user], function (err) {
        if (err) {
            handleError(err);
            return;
        }
        callback();
    });
}

// Fonction qui permet de récupérer tous les utilisateurs enregistrés
module.exports.getUsers = function (callback) {
    conn.query('SELECT * FROM user;', function(err, results) {
        if (err) {
            handleError(err);
            return;
        }
        callback(results);
    });
}

// Fonction qui permet de récupérer tous les vehicules enregistrés
module.exports.getVehicules = function (callback) {
    conn.query('SELECT * FROM vehicule;', function(err, results) {
        if (err) {
            handleError(err);
            return;
        }
        callback(results);
    });
}

// Fonction qui permet de récupérer le vehicule si il existe.
module.exports.getIsVehicule = function (vehicule, callback) {
    conn.query('SELECT * FROM vehicule WHERE nomVehicule = ?;', [vehicule], function(err, results) {
        if (err) {
            handleError(err);
            return;
        }
        if (results.length === 1)
            callback(results[0], true);
        else
            callback(null, false);
    });
}

// Fonction qui permet de récupérer le vehicule par son id si il existe.
module.exports.getIsIdVehicule = function (idVehicule, callback) {
    conn.query('SELECT * FROM vehicule WHERE idVehicule = ?;', [idVehicule], function(err, results) {
        if (err) {
            handleError(err);
            return;
        }
        if (results.length === 1)
            callback(results[0], true);
        else
            callback(null, false);
    });
}

// Fonction qui permet de créer un vehicule.
module.exports.createVehicule = function (vehicule, color, callback) {
    conn.query('INSERT INTO vehicule(nomVehicule,color) VALUES (?,?);', [vehicule, color], function (err) {
        if (err) {
            handleError(err);
            return;
        }
        callback();
    });
}

// Fonction qui permet de supprimer un vehicule.
module.exports.deleteVehicule = function (vehicule, callback) {
    conn.query('DELETE FROM vehicule WHERE nomVehicule = ?;', [vehicule], function (err) {
        if (err) {
            handleError(err);
            return;
        }
        callback();
    });
}

// Fonction qui permet de mettre à jour les informations du vehicule.
module.exports.updateInfoVehicule = function (vehicule, color, newVehicule, callback) {
    conn.query('UPDATE vehicule SET nomVehicule = ?, color = ? WHERE nomVehicule = ?;', [vehicule, color, newVehicule], function (err) {
        if (err) {
            handleError(err);
            return;
        }
        callback();
    });
}

// Fonction qui permet de récupérer les pré-requis pour la navigation.
module.exports.getRequirement = function (user, callback) {
    module.exports.getIsUser(user, function (userSQL, isUser) {
        if (isUser) {
            module.exports.getVehicules(function (vehicules) {
                callback(vehicules, userSQL, isUser);
            });
        } else {
            callback(null, null, isUser);
        }
    });
}

// Fonction qui permet de récupérer toutes les localisations enregistrés
module.exports.getLocs = function (callback) {
    conn.query('SELECT * FROM locs JOIN vehicule USING (idVehicule);', function(err, results) {
        if (err) {
            handleError(err);
            return;
        }
        callback(results);
    });
}

// Fonction qui permet de créer un point lié a un vehicule.
module.exports.createLoc = function (lat, lng, dateLoc, idVehicule, callback) {
    conn.query(`INSERT INTO locs(dateLoc, latLoc, longLoc, idVehicule) VALUES (?,?,?,?);`, [dateLoc, lat, lng, idVehicule], function (err) {
        if (err) {
            handleError(err);
            return;
        }
        callback();
    });
}

// Fonction qui permet de créer plusieurs points lié a un vehicule.
module.exports.createLocs = function (points, idVehicule, callback) {
    conn.query(`INSERT INTO locs(dateLoc, latLoc, longLoc, idVehicule) VALUES ${toStringQuery(points, idVehicule)};`, [idVehicule], function (err) {
        if (err) {
            handleError(err);
            return;
        }
        callback();
    });
}

// Fonction qui permet de récuperer les localisations d'un vehicule.
module.exports.getLocsByIdVehicule = function (idVehicule, callback) {
    conn.query('SELECT * FROM locs WHERE idVehicule = ?;', [idVehicule], function(err, results) {
        if (err) {
            handleError(err);
            return;
        }
        callback(results);
    });
}

// Connexion a la base de données (A le droit d'échoué)
module.exports.connection();

module.exports.conn = conn;