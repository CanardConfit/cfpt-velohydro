/*

Projet : VeloHydro
Description : Route pour la gestion des maps (trajets)

 */
let express = require('express');
let router = express.Router();
let db=require('../database');
let settings=require('../settings');

/*

Enregistrement des routes

 */

// Route pour la carte globale.
router.get('/all', function(req, res, next) {

    if (!settings.isAutorised(req.session, "/map/all", res)) {return;}

    db.getLocs(function (locs) {
        db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
            if (isUser) {

                let map = [];

                vehicules.forEach((v)=>{
                    let points = [];
                    locs.forEach((point)=> {
                        if (v.idVehicule === point.idVehicule) {
                            points.push(point);
                        }
                    });
                    map.push(points);
                });

                res.render('map/all', {activate: "all", vehicules: vehicules, data: map, session: userSQL});
            }
        });
    });
});

// Route pour la carte d'un vehicule specifique.
router.get('/view/:idVehicule', function(req, res, next) {

    let vehicule = req.params.idVehicule;

    if (!settings.isAutorised(req.session, "/map/view/" + vehicule, res)) {return;}

    db.getIsIdVehicule(vehicule, function (vehiculeSQL, isVehicule) {
        if (isVehicule) {
            db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
                if (isUser) {
                    db.getLocsByIdVehicule(vehicule, function (locs) {
                        res.render('map/vlv', {activate: "view" + vehicule, vehicules: vehicules, vehicule: vehiculeSQL, locData: locs, session: userSQL});
                    });
                }
            });
        } else res.redirect("/");
    });
});

// Route pour l'affichage de l'ajout de données manuelle d'un vehicule.
router.get('/view/:idVehicule/add', function(req, res, next) {

    let vehicule = req.params.idVehicule;

    if (!settings.isAutorised(req.session, "/map/view/" + vehicule + "/add", res)) {return;}

    db.getIsIdVehicule(vehicule, function (vehiculeSQL, isVehicule) {
        if (isVehicule) {
            db.getRequirement(req.session.username, function (vehicules, userSQL, isUser) {
                if (isUser) {
                    db.getLocsByIdVehicule(vehicule, function (locs) {
                        res.render('map/alv', {activate: "view" + vehicule, vehicules: vehicules, vehicule: vehiculeSQL, locData: locs, session: userSQL});
                    });
                }
            });
        } else res.redirect("/");
    });
});

// Route pour l'ajout de données manuelle d'un vehicule (Redirection).
router.post('/view/:idVehicule/add', function(req, res, next) {

    let vehicule = req.params.idVehicule;

    if (!settings.isAutorised(req.session, "/map/view/" + vehicule + "/add", res)) {return;}

    db.getIsIdVehicule(vehicule, function (vehiculeSQL, isVehicule) {
        if (isVehicule) {
            if (req.body.req_save) {
                let points = JSON.parse(req.body.latlongPoints);
                if (points.length > 0) {
                    db.createLocs(points, vehicule, function () {
                        res.redirect(`/map/view/${vehicule}`);
                    });
                } else res.redirect(`/map/view/${vehicule}`);
            } else  res.redirect(`/map/view/${vehicule}`);
        } else res.redirect("/");
    });
});

module.exports = router;